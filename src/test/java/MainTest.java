import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {
    private final String testCard = "1782938475647387";


    @Test
    void testSuccessIsNumber() {
        String onlyNumbers = "27161919102746";
        assertTrue(Main.isNumber(onlyNumbers));
    }

    @Test
    void testFailIsNumber(){
        String withCharacters = "18274918,1";
        assertFalse(Main.isNumber(withCharacters));
        assertFalse(Main.isNumber(""));
    }

    @Test
    void testSuccessGetCheckDigit() {
        assertEquals(Main.getCheckDigit(testCard), "7");
    }

    @Test
    void testFailGetCheckDigit(){
        assertEquals(Main.getCheckDigit(""), "");
    }

    @Test
    void testSuccessGetTrimmedDigits() {
        assertEquals(Main.getTrimmedDigits(testCard), "178293847564738");
    }

    @Test
    void testFailGetTrimmedDigits() {
        assertEquals(Main.getTrimmedDigits(""), "");
    }

    @Test
    void testSuccessIsValidCheckSum() {
        assertEquals(Main.isValidCheckSum("7", 7), "Valid");
    }
    @Test
    void testFailIsValidCheckSum() {
        assertEquals(Main.isValidCheckSum("7", 3), "Invalid");
        assertThrows(NumberFormatException.class, () -> {
            Main.isValidCheckSum("Hello", 0);
        });
    }

    @Test
    void testSuccessGetCheckSum() {
        assertEquals(Main.getCheckSum("178293847564738", "7"), 7);
        assertNotEquals(Main.getCheckSum("178293847564738", "3"), 3);
    }

    @Test
    void testFailGetCheckSum() {
        assertEquals(Main.getCheckSum("", "0"), -1);
    }

    @Test
    void testSuccessLongEnoughForCreditCard() {
        assertTrue(Main.longEnoughForCreditCard(testCard));
    }

    @Test
    void testFailLongEnoughForCreditCard() {
        assertFalse(Main.longEnoughForCreditCard(testCard+"3"));
    }
}