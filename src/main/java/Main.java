import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        System.out.print("\nWrite in the number to check: ");
        String number = sc.nextLine();


        while(!isNumber(number) || !longEnoughForCreditCard(number)){
            System.out.println("Not a number or too short!");
            System.out.print("\nNumber: ");
            number = sc.nextLine();
        }

        String trimmedDigits = getTrimmedDigits(number);
        String checkDigit = getCheckDigit(number);
        int calculatedCheckSum = getCheckSum(trimmedDigits, checkDigit);

        System.out.println("\nInput: " + trimmedDigits + " " + checkDigit);
        System.out.println("Provided: " + checkDigit);
        System.out.println("Expected: " + calculatedCheckSum);

        System.out.println("\n\nChecksum: " + isValidCheckSum(checkDigit, calculatedCheckSum));
        System.out.println("Digits: " + number.length() + (longEnoughForCreditCard(number) ? "(credit card)" : ""));
    }

    /**
     * Check if a String is a number
     * @param input the number to check
     * @return true if string is a number, false otherwise
     */
    public static boolean isNumber(String input){
        if(input.length() == 0){
            System.out.println("Length of digits cannot be 0!");
            return false;
        }

        // test each character for a number
        for(int i=0; i < input.length(); i++){
            try{
                Integer.parseInt(String.valueOf(input.charAt(i)));
            } catch(NumberFormatException ex){
                return false;
            }
        }
        return true;
    }

    /**
     * Get the last digit of the number to check
     * @param original The original number
     * @return the last digit of number or empty string if empty string provided
     */
    public static String getCheckDigit(String original){
        if(original.length() == 0){
            System.out.println("Length of digits cannot be 0!");
            return "";
        }

        return original.substring(original.length()-1);
    }

    /**
     * Get all digits except the last one
     * @param original The original number
     * @return all digits except the last one or empty string if empty string provided
     */
    public static String getTrimmedDigits(String original){
        if(original.length() == 0){
            System.out.println("Length of digits cannot be 0!");
            return "";
        }

        return original.substring(0, original.length()-1);
    }

    /**
     * Get if the check sum digit is equal to the expected value
     * @param provided the expected value
     * @param calculated the calculated value
     * @return "Valid" if values are equal, "Invalid" if not
     */
    public static String isValidCheckSum(String provided, int calculated){
        return Integer.parseInt(provided) == calculated ? "Valid" : "Invalid";
    }

    /**
     * Do a Luhn algorithm check
     * @param numbers the first numbers to check (except last digit)
     * @param checkDigit the digit to check the sum against
     * @return true if checksum of numbers equals checkDigit, false otherwise
     */
    public static int getCheckSum(String numbers, String checkDigit){
        if(numbers.length() == 0){
            System.out.println("Length of numbers cannot be 0!");
            return -1;
        }

        int sum = 0;
        int x = 0;
        String reverseNumber = new StringBuilder(numbers).reverse().toString();

        // start from left
        for(int i = 0; i < reverseNumber.length(); i++){
            int digit = Integer.parseInt(String.valueOf(reverseNumber.charAt(i)));

            // double the digits
            if(i % 2 == 0){
                if(digit * 2 <= 9)
                    sum += digit * 2;
                else
                    sum += (digit * 2) - 9;
            } else {
                sum += digit;
            }
        }
        x = (sum * 9) % 10;

        return x;
    }

    /**
     * Check if the numbers are long enough for a credit card
     * @param numbers the numbers to check
     * @return true if length of numbers is 16, false otherwise
     */
    public static boolean longEnoughForCreditCard(String numbers){
        return numbers.length() == 16;
    }
}
