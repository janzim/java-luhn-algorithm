# Java Luhn Algorithm
A Java implementation of the Luhn ALgorithm checking if a 16 digit number (credit card number) has been correctly entered.

## Showcase

### Correct input of 16 digits
![](gitlab-files/1.PNG)

### Invalid input of 16 digits
![](gitlab-files/2.PNG)